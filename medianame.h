#include <sys/time.h>

static inline unsigned long long time_usec(void)
{
	struct timeval t;
	gettimeofday(&t, NULL);

	return t.tv_sec * 1000000 + t.tv_usec;
}

static inline void get_name(char *buf, char *dir, char *templ)
{
	sprintf(buf, "%s/%lld.%s", dir, time_usec(), templ);
}
