#pragma once

#include "state.h"
#include <libmegapixels.h>
#include <stdbool.h>
#include <stdint.h>

void mp_io_pipeline_start();
void mp_io_pipeline_stop();

void mp_io_pipeline_focus();
void mp_io_pipeline_capture();
void mp_io_pipeline_set_control_int32(uint32_t control, uint32_t value);

void mp_io_pipeline_release_buffer(uint32_t buffer_index);

void mp_io_pipeline_update_state(const mp_state_io *state);
