#pragma once

#include "camera.h"
#include "state.h"
#include <gtk/gtk.h>

typedef struct _GdkSurface GdkSurface;

struct mp_process_pipeline_state {
        libmegapixels_camera *camera;
        libmegapixels_devconfig *configuration;

        int burst_length;

        int preview_width;
        int preview_height;

        int device_rotation;

        bool gain_is_manual;
        int gain;
        int gain_max;

        float balance_red;
        float balance_blue;

        bool exposure_is_manual;
        int exposure;

        bool has_auto_focus_continuous;
        bool has_auto_focus_start;

        bool flash_enabled;

        bool control_gain;
        bool control_exposure;
        bool control_flash;
        bool control_focus;
};

bool mp_process_find_processor(char *script, char *filename);
void mp_process_find_all_processors(GtkListStore *store);

void mp_process_pipeline_start();
void mp_process_pipeline_stop();
void mp_process_pipeline_sync();

void mp_process_pipeline_init_gl(GdkSurface *window);

void mp_process_pipeline_process_image(MPBuffer buffer);
void mp_process_pipeline_capture();
void mp_process_pipeline_update_state(const mp_state_proc *new_state);

void on_movie_start(void);
void on_movie_stop(void);

typedef struct _MPProcessPipelineBuffer MPProcessPipelineBuffer;

void mp_process_pipeline_buffer_ref(MPProcessPipelineBuffer *buf);
void mp_process_pipeline_buffer_unref(MPProcessPipelineBuffer *buf);
uint32_t mp_process_pipeline_buffer_get_texture_id(MPProcessPipelineBuffer *buf);
