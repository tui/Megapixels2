#pragma once

#include "gtk/gtk.h"
#include "process_pipeline.h"
#include "state.h"
#include "zbar_pipeline.h"

#define APP_ID "me.gapixels.Megapixels"

struct mp_main_state {
        const struct mp_camera_config *camera;
        libmegapixels_mode *mode;

        bool gain_is_manual;
        int gain;
        int gain_max;

        bool exposure_is_manual;
        int exposure;

        bool has_auto_focus_continuous;
        bool has_auto_focus_start;

        int image_width;
        int image_height;
};

void mp_main_update_state(const mp_state_main *new_state);

void mp_main_set_preview(MPProcessPipelineBuffer *buffer);
void mp_main_capture_completed(GdkTexture *thumb, const char *fname);

void mp_main_set_zbar_result(MPZBarScanResult *result);

int remap(int value, int input_min, int input_max, int output_min, int output_max);

bool check_window_active();

void notify_movie_message(gchar *msg);
